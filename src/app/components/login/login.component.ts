import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

    public username?: string;
    public password?: string;

    constructor(private router: Router, private auth: AuthService) { }

    public ngOnInit(): void {
        if(this.auth.isAuthenticated()) {
            this.router.navigate(['home']);
        }
    }

    public login(): void {
        if(!!this.username && !!this.password) {
            this.auth.login(this.username);
            this.router.navigate(['home']);
        }
    }
}
