import { Component } from '@angular/core';

import { AbstractSearchComponent } from 'src/app/components/shared/abstract-search.component';

@Component({
    selector: 'app-character-search',
    templateUrl: './character-search.component.html'
})
export class CharacterSearchComponent extends AbstractSearchComponent { }
