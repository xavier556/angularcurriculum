import { Component } from '@angular/core';
import { Character } from 'src/app/models/character';

import { AbstractListComponent } from 'src/app/components/shared/abstract-list.component';

@Component({
    selector: 'app-character-list',
    templateUrl: './character-list.component.html'
})
export class CharacterListComponent extends AbstractListComponent<Character> {
    public override endpoint: string = 'characters';
}
