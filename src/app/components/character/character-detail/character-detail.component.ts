import { Component } from '@angular/core';

import { Character } from 'src/app/models/character';
import { AbstractDetailComponent } from '../../shared/abstract-detail.component';

@Component({
    selector: 'app-character-detail',
    templateUrl: './character-detail.component.html'
})
export class CharacterDetailComponent extends AbstractDetailComponent<Character> {
    public override endpoint = "characters";
}