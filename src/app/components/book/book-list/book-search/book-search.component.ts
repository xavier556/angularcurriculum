import { Component } from '@angular/core';

import { AbstractSearchComponent } from 'src/app/components/shared/abstract-search.component';

@Component({
    selector: 'app-book-search',
    templateUrl: './book-search.component.html'
})
export class BookSearchComponent extends AbstractSearchComponent { }
