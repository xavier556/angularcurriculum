import { Component } from '@angular/core';

import { Book } from 'src/app/models/book';
import { AbstractListComponent } from 'src/app/components/shared/abstract-list.component';

@Component({
    selector: 'app-book-list',
    templateUrl: './book-list.component.html'
})
export class BookListComponent extends AbstractListComponent<Book> {
    public endpoint: string = 'books';
}
