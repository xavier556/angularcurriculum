import { Component } from '@angular/core';

import { Book } from 'src/app/models/book';
import { AbstractDetailComponent } from '../../shared/abstract-detail.component';

@Component({
    selector: 'app-book-detail',
    templateUrl: './book-detail.component.html'
})
export class BookDetailComponent extends AbstractDetailComponent<Book> {
    public override endpoint = 'books';
}
