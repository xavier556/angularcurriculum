import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Book } from 'src/app/models/book';
import { Character } from 'src/app/models/character';
import { House } from 'src/app/models/house';
import { Item, ItemType } from 'src/app/models/item';
import { ApiClientService } from 'src/app/services/api-client.service';

@Component({
    selector: 'app-item-card',
    templateUrl: './item-card.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemCardComponent<T extends Item> {

    @Input() public set url(value: string) {
        this._url = value;
        this.update();
    }

    public item$ = new Observable<Item>();
    public route: string = '';

    private _type: ItemType = ItemType.unknown;
    private _url: string = '';

    constructor(private client: ApiClientService) { }

    private update() {
        this.item$ = this.client.get<T>(this._url).data$;
        let parts = this._url.split('/');
        this.route = '/' + parts.slice(-2).join('/');
        this._type = ItemType[parts[parts.length - 2] as keyof typeof ItemType];
    }
        
    public getName(item: any): string {
        switch(this._type) {
            case ItemType.books: return (item as Book).name;
            case ItemType.characters: return (item as Character).name || (item as Character).aliases[0];
            case ItemType.houses: return (item as House).name;
            default: return '';
        }
    }

    public getBlob(item: any): string {
        switch(this._type) {
            case ItemType.books: return (item as Book).released.toString().slice(0, 10);
            case ItemType.characters: return (item as Character).aliases[0];
            case ItemType.houses: return (item as House).region;
            default: return '';
        }
    }

    public getImage(): string {
        switch(this._type) {
            case ItemType.books: return '/assets/book.png';
            case ItemType.characters: return '/assets/character.png';
            case ItemType.houses: return '/assets/house.png';
            default: return '';
        }
    }
}
