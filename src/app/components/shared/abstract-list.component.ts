import { Directive, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, map, Observable, Subject, switchMap, takeUntil } from 'rxjs';

import { Pagination } from 'src/app/models/pagination';
import { ApiParams } from 'src/app/models/parameters';
import { ApiResponse } from 'src/app/services/api-response';
import { ApiClientService } from 'src/app/services/api-client.service';
import { Item } from '../../models/item';

@Directive()
export abstract class AbstractListComponent<T extends Item> implements OnInit, OnDestroy {

    public abstract endpoint: string;
    public params: ApiParams = { page: '1', pageSize: '6' };
    
    private ngUnsubscribe$ = new Subject();
    private request$: BehaviorSubject<ApiParams> = new BehaviorSubject(this.params);
    public response$!: Observable<ApiResponse<T[]>>;

    get items$(): Observable<T[]> {
        return this.response$?.pipe(switchMap(r => r.data$));
    }

    get pagination$(): Observable<Pagination> {
        return this.response$.pipe(switchMap(r => r.pagination$));
    }

    constructor(private client: ApiClientService) { }

    public ngOnInit(): void {
        this.response$ = this.request$.pipe(
            takeUntil(this.ngUnsubscribe$),
            map(params => this.client.get<T[]>(this.endpoint, params))
        );
    }

    public ngOnDestroy(): void {
        this.ngUnsubscribe$.next(1);
        this.ngUnsubscribe$.complete();
    }

    public search(params?: ApiParams): void {
        this.params = { ...this.params, ...params }
        this.request$.next(this.params);
        this.toggle('search', true);
    }

    public reset(search: boolean = false): void {
        this.params = {};
        if(search) { this.search(); }
    }

    public toggle(id: string, force?: boolean): void {
        document.getElementById(id)?.toggleAttribute('collapsed', force);
    }
}
