import { Pagination } from "src/app/models/pagination";
import { PaginationComponent } from "./pagination.component";

describe('PaginationComponent', () => {
    

    let sut = new PaginationComponent();
    sut.pagination = { current: 1, last: 3 } as Pagination;

    describe('next', () => {
        it('should return current page +1', () => {
            sut.pagination!.current = 1;
            expect(sut.next).toEqual(2);
        });
        it('should return a maximum of last', () => {
            sut.pagination!.current = sut.last;
            expect(sut.next).toEqual(sut.last);
        });
    });

    describe('prev', () => {
        it('shoud return current page -1', () => {
            sut.pagination!.current = 2;
            expect(sut.prev).toEqual(1);
        });
        it('should return a minimum of 1', () => {
            sut.pagination!.current = 1;
            expect(sut.prev).toEqual(1);
        });
    });

});
