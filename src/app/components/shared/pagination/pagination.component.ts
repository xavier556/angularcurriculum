import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Pagination } from 'src/app/models/pagination';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html'
})
export class PaginationComponent {

    @Input() public pagination: Pagination | null = null;

    @Output() public selectPage = new EventEmitter<number>();
    
    public get prev() {
        return Math.max(this.current - 1 , 1);
    }

    public get next() {
        return Math.min(this.current + 1, this.last);
    }

    public get last() {
        return this.pagination?.last ?? 1;
    }

    public get current() {
        return this.pagination?.current ?? 1;
    }

    public get size() {
        return this.pagination?.size ?? 1;
    }

    public get total() {
        return this.pagination?.items || (this.size * this.last);
    }

    public onSelectPage(page?: number): void {
        this.selectPage.emit(page);
    }
}
