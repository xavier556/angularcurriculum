import { Directive, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject, takeUntil } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { ApiClientService } from 'src/app/services/api-client.service';
import { Item } from '../../models/item';

@Directive()
export abstract class AbstractDetailComponent<T extends Item> implements OnInit, OnDestroy {

    public abstract endpoint: string;
    
    public item$ = new Observable<T>();
    private ngUnsubscribe$ = new Subject();
    
    constructor(
        private client: ApiClientService,
        private route: ActivatedRoute
    ) { }

    public ngOnInit(): void {
        this.route.paramMap.pipe(
            takeUntil(this.ngUnsubscribe$)
        ).subscribe(params => {
            this.item$ = this.client.get<T>(this.endpoint + '/' + params.get('id')).data$;
        });
    }
    
    public ngOnDestroy(): void {
        this.ngUnsubscribe$.next(true);
        this.ngUnsubscribe$.complete();
    }
}
