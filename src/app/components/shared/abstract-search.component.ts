import { Directive, EventEmitter, Input, Output } from "@angular/core";

import { ApiParams } from "../../models/parameters";

@Directive()
export abstract class AbstractSearchComponent {
    
    @Input() public params!: ApiParams;

    @Output() public search = new EventEmitter<ApiParams>();
    @Output() public clear = new EventEmitter();
    
    public onSearch(): void {
        this.params['page'] = 1;
        this.search.emit(this.params);
    }

    public onReset(): void {
        this.params = {};
        this.clear.emit();
    }
}
