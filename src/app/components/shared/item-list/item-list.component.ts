import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Pagination } from 'src/app/models/pagination';

@Component({
    selector: 'app-item-list',
    templateUrl: './item-list.component.html'
})
export class ItemListComponent implements OnInit {

    @Input() public urls: string[] = [];
    @Input() public pageSize: number = 1;
    
    public urlsSlice: string[] = [];
    private pag: Pagination = {} as Pagination;
    public pag$ = new BehaviorSubject<Pagination>(this.pag);

    ngOnInit(): void {
        
        this.pag = {
            items: this.urls.length,
            current: 1,
            last: Math.ceil(this.urls.length / this.pageSize),
            size: this.pageSize
        }
        
        this.getPage(1);
    }

    public getPage(page: number): void {
        this.pag.current = page;
        this.urlsSlice = this.urls.slice((this.pag.current - 1) * this.pag.size, this.pag.current * this.pag.size)
        this.pag$.next(this.pag);
    }
}
