import { Component } from '@angular/core';

import { House } from 'src/app/models/house';
import { AbstractListComponent } from 'src/app/components/shared/abstract-list.component';

@Component({
    selector: 'app-house-list',
    templateUrl: './house-list.component.html'
})
export class HouseListComponent extends AbstractListComponent<House> {
    public override endpoint: string = "houses";
}
