import { Component } from '@angular/core';
import { AbstractSearchComponent } from 'src/app/components/shared/abstract-search.component';

@Component({
  selector: 'app-house-search',
  templateUrl: './house-search.component.html'
})
export class HouseSearchComponent extends AbstractSearchComponent { }
