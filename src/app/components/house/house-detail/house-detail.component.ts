import { Component } from '@angular/core';

import { House } from 'src/app/models/house';
import { AbstractDetailComponent } from '../../shared/abstract-detail.component';

@Component({
    selector: 'app-house-detail',
    templateUrl: './house-detail.component.html'
})
export class HouseDetailComponent extends AbstractDetailComponent<House> {
    public override endpoint = 'houses';
}