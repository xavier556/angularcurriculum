import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    title = 'gotapp';

    constructor(private router: Router, private auth: AuthService) { }

    public getActivePage(): string {
        if (this.router.url.startsWith('/books')) return 'books';
        if (this.router.url.startsWith('/characters')) return 'characters';
        if (this.router.url.startsWith('/houses')) return 'houses';
        else return 'home';
    }

    public getUsername(): string {
        return this.auth.getUsername();
    }

    public logout(): void {
        this.auth.logout();
        this.router.navigate(['login']);
    }
}
