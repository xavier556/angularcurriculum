import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BookListComponent } from './components/book/book-list/book-list.component';
import { BookDetailComponent } from './components/book/book-detail/book-detail.component';
import { CharacterListComponent } from './components/character/character-list/character-list.component';
import { CharacterDetailComponent } from './components/character/character-detail/character-detail.component';
import { HouseListComponent } from './components/house/house-list/house-list.component';
import { HouseDetailComponent } from './components/house/house-detail/house-detail.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuardService } from './services/auth-guard';
import { LoginComponent } from './components/login/login.component';


const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'books',
        component: BookListComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'books/:id',
        component: BookDetailComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'characters',
        component: CharacterListComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'characters/:id',
        component: CharacterDetailComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'houses',
        component: HouseListComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'houses/:id',
        component: HouseDetailComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: '**',
        redirectTo: '/'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'top'})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
