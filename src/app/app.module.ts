import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookListComponent } from './components/book/book-list/book-list.component';
import { BookSearchComponent } from './components/book/book-list/book-search/book-search.component';
import { BookDetailComponent } from './components/book/book-detail/book-detail.component';
import { CharacterListComponent } from './components/character/character-list/character-list.component';
import { CharacterSearchComponent } from './components/character/character-list/character-search/character-search.component';
import { CharacterDetailComponent } from './components/character/character-detail/character-detail.component';
import { HouseListComponent } from './components/house/house-list/house-list.component';
import { HouseSearchComponent } from './components/house/house-list/house-search/house-search.component';
import { HouseDetailComponent } from './components/house/house-detail/house-detail.component';
import { PaginationComponent } from './components/shared/pagination/pagination.component';
import { HomeComponent } from './components/home/home.component';
import { ItemListComponent } from './components/shared/item-list/item-list.component';
import { ItemCardComponent } from './components/shared/item-card/item-card.component';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard';
import { LoginComponent } from './components/login/login.component';


@NgModule({
    declarations: [
        AppComponent,
        BookListComponent,
        BookDetailComponent,
        BookSearchComponent,
        CharacterListComponent,
        CharacterSearchComponent,
        CharacterDetailComponent,
        HouseListComponent,
        HouseSearchComponent,
        HouseDetailComponent,
        PaginationComponent,
        HomeComponent,
        ItemListComponent,
        ItemCardComponent,
        LoginComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [
        AuthService, 
        AuthGuardService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
