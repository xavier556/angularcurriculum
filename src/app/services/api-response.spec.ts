import { HttpHeaders, HttpResponse } from "@angular/common/http";
import { waitForAsync } from "@angular/core/testing";
import { of } from "rxjs";
import { ApiResponse } from "./api-response";

describe('response', () => {

    let fakeResponse: ApiResponse<unknown>;

    beforeAll(() => {

        const link = '<foo>; rel="bar",<https://www.anapioficeandfire.com/api/books?page=999&pageSize=10>; rel="last",<bar>; rel="foo"';
        const httpResponse = new HttpResponse<unknown>({headers: new HttpHeaders({link: link})});
        fakeResponse = new ApiResponse(of(httpResponse));
    });

    describe('extractLink', () => {

        it('should extract last page from well formed link header', waitForAsync(() => {
            fakeResponse.pagination$.subscribe(pagination => {
                expect(pagination.last).toEqual(999);
            });
        }));

    });

});
