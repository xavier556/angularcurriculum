import { HttpHeaders, HttpResponse } from "@angular/common/http";
import { map, Observable } from "rxjs";

import { Pagination } from "../models/pagination";

export class ApiResponse<T> {

    public data$: Observable<T>;
    public pagination$: Observable<Pagination>;

    constructor(response: Observable<HttpResponse<T>>) {

        this.data$ = response.pipe(
            map(response => response?.body as T)
        );

        this.pagination$ = response.pipe(
            map(response => ApiResponse.extractLink(response.headers))
        );
    }

    private static extractLink(headers: HttpHeaders): Pagination {
        const link = headers.get('link');
        if (!link) { return {} as Pagination; }

        // regexp to extract the page parameter and rel value of the links
        // ex: <https://www.anapioficeandfire.com/api/books?page=1&pageSize=10>; rel="prev"
        const rxLast = /.*<.*page=(\d+)&pageSize=(\d+)>.*rel="last".*/;
        const resLast = rxLast.exec(link) || [];
        const last = resLast.length > 1 ? resLast[1] : 0;
        const size = resLast.length > 1 ? resLast[2] : 0;
        const rxCurrent = /.*<.*page=(\d+).*>.*rel="next".*/;
        const resCurrent = rxCurrent.exec(link) || [];
        const current = resCurrent?.length === 2 ? +resCurrent[1] - 1 : last;
        return { last: +last, current: +current, size: +size, items: 0 };
    }
}