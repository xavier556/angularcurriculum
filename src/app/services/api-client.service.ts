import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, shareReplay } from 'rxjs';

import { ApiResponse } from './api-response';
import { ApiParams } from '../models/parameters';

@Injectable({
    providedIn: 'root'
})
export class ApiClientService {

    private readonly root: string = "https://www.anapioficeandfire.com/api";

    private cache: { [url: string]: Observable<any> } = {};

    constructor(private httpClient: HttpClient) { }

    public getRoot(): Observable<any> {
        return this.httpClient.get(this.root, {});
    }

    public get<T>(endpoint: string, params?: ApiParams): ApiResponse<T> {
        let url = endpoint.indexOf("anapioficeandfire.com/api") > 0  ? endpoint : this.root + '/' + endpoint;
        if(params) { url += '?' + this.queryString(params) }    // add query string parameters if params is provided
        return new ApiResponse(this.fromCache<HttpResponse<T>>(url,
            this.httpClient.get<T>(url, { observe: 'response' }).pipe(shareReplay(1))));
    }
    
    private fromCache<T>(key: string, obs: Observable<T>): Observable<T> {
        if (!this.cache[key]) {
            this.cache[key] = obs;
        }
        return this.cache[key];
    }

    private queryString = (params: ApiParams): string =>
        Object.keys(params).filter(k => !!params[k]).map(key => key + '=' + params[key]).join('&');
}
