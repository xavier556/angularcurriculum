import { Injectable } from '@angular/core';

const AUTH_TOKEN_NAME = 'angular-curriculum-auth-token';

@Injectable()
export class AuthService {
    
    public isAuthenticated(): boolean {
        const token = localStorage.getItem(AUTH_TOKEN_NAME) || '';
        return !!token;
    }

    public login(username: string): void {
        localStorage.setItem(AUTH_TOKEN_NAME, username);
    }

    public logout(): void {
        localStorage.removeItem(AUTH_TOKEN_NAME);
    }

    public getUsername(): string {
        return this.isAuthenticated() ? localStorage.getItem(AUTH_TOKEN_NAME) || '' : '';
    }
}