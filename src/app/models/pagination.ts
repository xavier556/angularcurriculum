export interface Pagination {
    last: number;
    current: number;
    size: number;
    items: number;
}