import { Item } from "./item";

export interface Book extends Item {
    isbn: string;
    authors: string[];
    numberOfPages: string;
    publisher: string;
    country: string;
    mediaType: string;
    released: Date;
    characters: string[];
    povCharacters: string[];
}
