export interface Item {
    url: string;
    name: string;
};

export enum ItemType {
    books = 'books',
    characters = 'characters',
    houses = 'houses',
    unknown = 'unknown'
}